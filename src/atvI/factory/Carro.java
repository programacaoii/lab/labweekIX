package atvI.factory;

public class Carro extends Veiculo {
    public int getCapacidadeCarga() {
        return 500;
    }

    public int getVelocidadeMaxima() {
        return 200;
    }
}

