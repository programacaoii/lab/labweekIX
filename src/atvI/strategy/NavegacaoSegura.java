package atvI.strategy;

public class NavegacaoSegura implements EstrategiaDeNavegacao {
    @Override
    public String calcularRota(String origem, String destino) {
        return "Rota segura de " + origem + " para " + destino;
    }
}
