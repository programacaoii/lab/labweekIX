package atvI.factory;

public class FabricaDeVeiculosConcreta implements FabricaDeVeiculos {
    public Veiculo criarVeiculo(String tipo) {
        if (tipo.equals("caminhao")) {
            return new Caminhao();
        } else if (tipo.equals("carro")) {
            return new Carro();
        } else if (tipo.equals("moto")) {
            return new Moto();
        } else {
            throw new IllegalArgumentException("Tipo de veículo desconhecido");
        }
    }
}
