package atvI.demonstracao;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import atvI.factory.FabricaDeVeiculos;
import atvI.factory.FabricaDeVeiculosConcreta;
import atvI.factory.Veiculo;
import atvI.strategy.EstrategiaDeNavegacao;
import atvI.strategy.NavegacaoRapida;
import atvI.strategy.NavegacaoSegura;
import atvI.strategy.VeiculoComEstrategia;

public class SistemaDeNavegacao {
    private Map<Integer, VeiculoComEstrategia> veiculos = new HashMap<>();
    private FabricaDeVeiculos fabrica = new FabricaDeVeiculosConcreta();
    private Scanner scanner = new Scanner(System.in);

    public void iniciar() {
    	 int escolha;

    	    do {
    	        exibirMenuPrincipal();
    	        escolha = scanner.nextInt();
    	        scanner.nextLine();

    	        switch (escolha) {
    	            case 1:
    	                criarVeiculo();
    	                break;
    	            case 2:
    	                escolherEstrategia();
    	                break;
    	            case 3:
    	                calcularRota();
    	                break;
    	            case 4:
    	                exibirVeiculos();
    	                break;
    	            case 5:
    	                System.out.println("Programa encerrado!");
    	                break;
    	            default:
    	                System.out.println("Opcao invalida. Tente novamente.");
    	        }
    	    } while (escolha != 5);
    	}

    private void exibirMenuPrincipal() {
        System.out.println("\nMenu Principal:");
        System.out.println("1 - Criar Veiculo");
        System.out.println("2 - Escolher Estrategia de Navegacao");
        System.out.println("3 - Calcular Rota");
        System.out.println("4 - Exibir Veiculos");
        System.out.println("5 - Sair");
        System.out.print("Escolha uma opcao: ");
    }

    private void criarVeiculo() {
        System.out.println("\nTipos de Veiculos Disponiveis:");
        System.out.println("1 - Caminhao");
        System.out.println("2 - Carro");
        System.out.println("3 - Moto");
        System.out.print("Escolha o tipo de veiculo (1/2/3): ");
        int escolha = scanner.nextInt();
        scanner.nextLine();

        String tipoVeiculo = "";

        switch (escolha) {
            case 1:
                tipoVeiculo = "caminhao";
                break;
            case 2:
                tipoVeiculo = "carro";
                break;
            case 3:
                tipoVeiculo = "moto";
                break;
            default:
                System.out.println("Tipo de veiculo invalido.");
                return;
        }

        Veiculo veiculo = fabrica.criarVeiculo(tipoVeiculo);
        VeiculoComEstrategia veiculoComEstrategia = new VeiculoComEstrategia(veiculo);
        veiculos.put(veiculos.size() + 1, veiculoComEstrategia);
        System.out.println(veiculo.toString());
    }

    private void escolherEstrategia() {
        exibirApenasVeiculos();
        System.out.print("Escolha o veiculo pelo numero: ");
        int numeroVeiculo = scanner.nextInt();
        scanner.nextLine();

        if (!veiculos.containsKey(numeroVeiculo)) {
            System.out.println("Veiculo nao encontrado.");
            return;
        }

        System.out.println("\nEstrategias de Navegacao Disponiveis:");
        System.out.println("1 - Navegacao Rapida");
        System.out.println("2 - Navegacao Segura");
        System.out.print("Escolha a estrategia de navegacao (1/2): ");
        int escolha = scanner.nextInt();
        scanner.nextLine();

        EstrategiaDeNavegacao estrategia = null;

        switch (escolha) {
            case 1:
                estrategia = new NavegacaoRapida();
                break;
            case 2:
                estrategia = new NavegacaoSegura();
                break;
            default:
                System.out.println("Opcao invalida.");
                return;
        }

        VeiculoComEstrategia veiculoComEstrategia = veiculos.get(numeroVeiculo);
        veiculoComEstrategia.definirEstrategiaNavegacao(estrategia);
        System.out.println("Estrategia de Navegacao definida para o veiculo " + numeroVeiculo + ": " + estrategia.getClass().getSimpleName());
    }

    private void calcularRota() {
        exibirApenasVeiculos();
        System.out.print("Escolha o veiculo pelo numero: ");
        int numeroVeiculo = scanner.nextInt();
        scanner.nextLine();

        if (!veiculos.containsKey(numeroVeiculo)) {
            System.out.println("Veiculo nao encontrado.");
            return;
        }

        System.out.print("Informe a origem: ");
        String origem = scanner.next();
        System.out.print("Informe o destino: ");
        String destino = scanner.next();

        VeiculoComEstrategia veiculoComEstrategia = veiculos.get(numeroVeiculo);
        String rota = veiculoComEstrategia.calcularRota(origem, destino);

        System.out.println("Rota calculada para o veiculo " + numeroVeiculo + ": " + rota);
    }

    private void exibirVeiculos() {
        if (veiculos.isEmpty()) {
            System.out.println("\nNenhum veiculo criado ainda.");
        } else {
            System.out.println("\nVeiculos Disponiveis:");
            for (Map.Entry<Integer, VeiculoComEstrategia> entrada : veiculos.entrySet()) {
                VeiculoComEstrategia veiculoComEstrategia = entrada.getValue();
                Veiculo veiculo = veiculoComEstrategia.getVeiculo();
                EstrategiaDeNavegacao estrategia = veiculoComEstrategia.getEstrategiaNavegacao();

      
                System.out.println("Veiculo: " + veiculo.getClass().getSimpleName());
                System.out.println("Rota Escolhida: " + veiculoComEstrategia.getRotaEscolhida());

                if (estrategia != null) {
                    System.out.println("Estrategia de Navegacao: " + estrategia.getClass().getSimpleName());
                } else {
                    System.out.println("Estrategia de Navegacao nao definida.");
                }
            }
        }
    }

    private void exibirApenasVeiculos() {
        if (veiculos.isEmpty()) {
            System.out.println("\nNenhum veiculo criado ainda.");
        } else {
            System.out.println("\nVeiculos Disponíveis:");
            for (Map.Entry<Integer, VeiculoComEstrategia> entrada : veiculos.entrySet()) {
                VeiculoComEstrategia veiculoComEstrategia = entrada.getValue();
                Veiculo veiculo = veiculoComEstrategia.getVeiculo();
                System.out.println(entrada.getKey() + " - " + veiculo.getClass().getSimpleName());
            }
        }
    }
}

