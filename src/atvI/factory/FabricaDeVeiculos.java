package atvI.factory;

public interface FabricaDeVeiculos {
    Veiculo criarVeiculo(String tipo);
}