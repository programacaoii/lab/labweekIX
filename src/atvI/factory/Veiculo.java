package atvI.factory;

public abstract class Veiculo {
    public abstract int getCapacidadeCarga();
    public abstract int getVelocidadeMaxima();
    
    @Override
    public String toString() {
        return "Veiculo criado: " + getClass().getSimpleName() +
               "\nCapacidade de Carga: " + getCapacidadeCarga() +
               "\nVelocidade Maxima: " + getVelocidadeMaxima();
    }
    
}

