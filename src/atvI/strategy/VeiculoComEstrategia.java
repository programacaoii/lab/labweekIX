package atvI.strategy;

import atvI.factory.Veiculo;

public class VeiculoComEstrategia {
    private Veiculo veiculo;
    private EstrategiaDeNavegacao estrategiaNavegacao;
    private String rotaEscolhida;

    public VeiculoComEstrategia(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public void definirEstrategiaNavegacao(EstrategiaDeNavegacao estrategiaNavegacao) {
        this.estrategiaNavegacao = estrategiaNavegacao;
    }

    public String calcularRota(String origem, String destino) {
        if (estrategiaNavegacao != null) {
            this.rotaEscolhida = estrategiaNavegacao.calcularRota(origem, destino);
            return rotaEscolhida;
        } else {
            return "Estratrgia de navegacao nao definida.";
        }
    }

    public EstrategiaDeNavegacao getEstrategiaNavegacao() {
        return estrategiaNavegacao;
    }

    public String getRotaEscolhida() {
        return rotaEscolhida;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }
    
}

