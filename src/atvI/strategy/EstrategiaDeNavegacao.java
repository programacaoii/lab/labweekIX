package atvI.strategy;

public interface EstrategiaDeNavegacao {
    String calcularRota(String origem, String destino);
}
