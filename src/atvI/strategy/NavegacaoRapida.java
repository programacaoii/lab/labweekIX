package atvI.strategy;

public class NavegacaoRapida implements EstrategiaDeNavegacao {
    @Override
    public String calcularRota(String origem, String destino) {
        return "Rota rápida de " + origem + " para " + destino;
    }
}